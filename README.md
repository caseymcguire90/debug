# Puget Experience

## Installation

Run the following commands to install the app

1. Install dependencies
`bundle install`

2. Set up the database
`rake db:create db:migrate db:seed`

3. Start the server
`rails s`

## Running the tests

`rspec spec/`

###Site URL
http://evening-lowlands-9692.herokuapp.com
