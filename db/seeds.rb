# -*- coding: utf-8 -*-
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

locations = [
             {:name => "Wheelock Student Center", 
               :logo_url => "http://www.pugetsound.edu/images/vt/buildings/thumb_dsc_8798.jpg", 
               :short_description => "Where dreams are won and lost",
               :long_description => "lorem ipsum",
               :pictures => ["http://www.pugetsound.edu/images/vt/buildings/Wheelock_3.jpg","http://www.pugetsound.edu/images/vt/buildings/_DSC5319.jpg","http://www.pugetsound.edu/images/vt/buildings/WheelockDiversionsTD022.jpg","http://www.pugetsound.edu/images/vt/buildings/wheelockfoodcourt_3057.jpg","http://www.pugetsound.edu/images/vt/buildings/WheelockTD072.jpg","http://www.pugetsound.edu/images/vt/buildings/Wheelock_Celler.jpg"],
               :latitude => "47.263325",
               :longitude => "-122.479282"
               },
               {
               :name => "Alcorn Arboretum",
               :logo_url =>  "http://www.pugetsound.edu/images/vt/buildings/_DSC9465.jpg",
               :short_description => "More than 2,000 trees dot the campus landscape. Meandering paths and walkways create leafy enclaves and resting spots.",
               :long_description => "The grounds of University of Puget Sound are designated an arboretum in honor of longtime biology professor Gordon Dee Alcorn, who was the unofficial landscape recorder during his tenure with the college. The Puget Sound campus is 97 acres of landscaped grounds and traditional Tudor-Gothic architecture. Campus planners have consciously worked to extend the \"wandering glen\" feel created by the canopy of Douglas firs on campus.",
               :pictures => ["http://www.pugetsound.edu/images/vt/buildings/Abor.jpg", "http://www.pugetsound.edu/images/vt/buildings/Abor_3.jpg", "http://www.pugetsound.edu/images/vt/buildings/Abor_4.jpg"],
               :latitude => "47.264203",
               :longitude => "-122.482072"
               
             },
             {
             :name => "Baker Stadium",
             :logo_url => "http://www.pugetsound.edu/images/vt/buildings/Athletics%20stadium.jpg",
             :short_description => "The stadium hosts Logger football, soccer, cross country, track and field, and women's lacrosse, as well as the college's Commencement Ceremony each May.",
             :long_description => "Baker Stadium has a covered seating capacity of 3000, uncovered seating for 500, and is located on the west side of campus, on N. 11th Street. The stadium hosts Logger football, soccer, cross country, track and field, and women's lacrosse, as well as the college's Commencement Ceremony each May. Student-athletes at Puget Sound participate in 23 varsity sports and compete in the NCAA Division III Northwest Conference (NWC). Logger athletic teams collectively and consistently rank in the top 15 percent nationally among more than 400 NCAA Division III teams.",
               :pictures => ["http://www.pugetsound.edu/images/vt/buildings/Athletics%20stadium.jpg", "http://www.pugetsound.edu/images/vt/buildings/stadium.jpg", "http://www.pugetsound.edu/images/vt/buildings/wm%20basketball%20team%6008_09_1.jpg", "http://www.pugetsound.edu/images/vt/buildings/Peytonfieldarial.jpg", "http://www.pugetsound.edu/images/vt/buildings/baker_football.jpg", "http://www.pugetsound.edu/images/vt/buildings/%20Peyton%20field%206814-27a.jpg"],
               :latitude => "47.260901",
               :longitude => "-122.481861"
             },
             {
               :name => "Wyatt Hall",
               :logo_url => "http://www.pugetsound.edu/images/vt/buildings/WyattTD056.jpg",
               :short_description => "The building houses various humanities departments, as well as offices for politics and government.",
               :long_description => "Built in 2000, Wyatt Hall is named for 23-year Puget Sound board of trustees member Lowry Wyatt. The building is home to offices for the humanities and politics and government departments, as well as classrooms. More than half of Wyatt Hall's classrooms are seminar style, accommodating the discussion-oriented teaching most common at Puget Sound. The building also has state-of-the-art slide lecture facilities, presentation rooms, and cutting-edge “turn-around” classrooms with computer terminals located around the perimeter.",
               :pictures => ["http://www.pugetsound.edu/images/vt/buildings/WyattTD026.jpg","http://www.pugetsound.edu/images/vt/buildings/wyatt_2.jpg","http://www.pugetsound.edu/images/vt/buildings/WyattJenniDenekas%6011.jpg","http://www.pugetsound.edu/images/vt/buildings/WyattTD011.jpg","http://www.pugetsound.edu/images/vt/buildings/Wyatt%20Hall.jpg"],
               :latitude => "47.261800",
               :longitude => "-122.482470"
             },
             {
               :name => "Warner Gymnasium",
               :logo_url => "http://www.pugetsound.edu/images/vt/buildings/Warner+Stadum.jpg",
               :short_description => "Warner Gym is home to Wallace Pool, a 25-meter indoor pool used by the Logger men's and women's swim teams as well as intramural and club teams, such as kayaking and water polo.",
               :long_description => "Though named for a location rather than for specific friends, faculty, or students of the University, Warner Gymnasium was one of the first buildings placed on campus during the university's move to its current location at 15th and Warner. It was completed in 1924 along with Jones Hall.",
               :pictures => ["http://www.pugetsound.edu/images/vt/buildings/Warner+Stadum.jpg","http://www.pugetsound.edu/images/vt/buildings/Warner%20Gym.jpg","http://www.pugetsound.edu/images/vt/buildings/Warner.jpg","http://www.pugetsound.edu/images/vt/buildings/WarnerGym_1.jpg"],
               :latitude => "47.261607",
               :longitude => "-122.481695"
             }
            ]

locations.each do |location|
  Location.create!(location)
end
