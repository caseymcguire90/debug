class LocationsController < ApplicationController
  layout "page"
  respond_to :json
  #information on the current location
  def info
    @location = Location.find(params[:id])
  end
  
  #menu view
  def menu
    @locations = Location.all
  end
  
  #shows current location on map
  def map
    @location = Location.find(params[:id])
    @all_locations = Location.all
  end
end
