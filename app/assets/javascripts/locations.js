//=require jquery
//=require ninja-slider
var map;
var markers;
var currentLocation;
var allLocations;
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
//centers the view over the given location
function changeLocation(lat, lon){

    //probably don't need these
    markerToLocation = {};
    markers = [];

    directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
    
    var mapOptions = {
	zoom: 19,
	center: new google.maps.LatLng(parseFloat(lat, 10), parseFloat(lon, 10)),
	disableDefaultUI: true
    }
    
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    directionsDisplay.setMap(map);  
}

//puts markers on the map
function getMapMarkers(locations){
    for(var i = 0; i < locations.length; i++){
        var curLat = parseFloat(locations[i].latitude, 10);
        var curLon = parseFloat(locations[i].longitude, 10);

        var curLatLng = new google.maps.LatLng(curLat, curLon);
        console.log(curLatLng);

        var curMarker = new google.maps.Marker({
            map: map,
            draggable: false,
            position: curLatLng
        });

        google.maps.event.addListener(curMarker, 'click', function(i){
            calcRoute(i);
        });
        
    }
}

function calcRoute(next){

    //console.log(currentLocation);
    //console.log(next);

    var start = new google.maps.LatLng(parseFloat(currentLocation.latitude, 10), parseFloat(currentLocation.longitude, 10));
    var end = next.latLng;

    console.log(start);
    console.log(end);
    var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.WALKING
    }
    console.log(request);
    directionsService.route(request, function(response, status){
        if(status == google.maps.DirectionsStatus.OK){

            directionsDisplay.setDirections(response);
        }
    });


    //console.log(start);
    //console.log(end);
}

$(document).ready(function(){
    currentLocation = $("#current-location").data('location');

    changeLocation(currentLocation.latitude, currentLocation.longitude);

    allLocations = $("#all-locations").data('locations');
    console.log(allLocations);
    getMapMarkers(allLocations);
    //console.log(currentLocation);
    //console.log(locations);
    //console.log(locations);

});

//google.maps.event.addDomListener(window, 'load', changeLocation);
