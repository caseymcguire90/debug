Rails.application.routes.draw do
  get 'locations/start'

  get '/tour/:id', to: 'locations#map', as: 'location'
  get '/tour_stop/:id', to: 'locations#info', as:'tour_stop'

  get 'tour' => 'locations#menu'

  get '/highlights', to: "highlights#show"

  root to: "home#show" 
  # get '/webcam', to: "webcam#show"

  get "/webcam", to: "pages#show"
  get "/kups", to: "pages#show"
  get "/social_media", to: "pages#show"
end
